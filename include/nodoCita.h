typedef struct NodoCita{
	Cita *date;
	struct NodoCita *next;
}NodoCita;

void push(NodoCita **head, Cita *cita);
void print_citas(NodoCita *head);
void deleteCita(NodoCita **head, int n);
void modificarCita(NodoCita **head, char *fecha, char *lug, char *inicio, char *fin, int n, Contacto *cont);
Cita * popCita(NodoCita **head);
Cita * getCita(NodoCita **head, int n);
