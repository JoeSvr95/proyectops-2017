struct Contacto{
	char nombre[20];
	char apellido[20];
	/* La fecha de nacimiento sería del formato dd/mm/yyyy */
	char fechaCump[12];
	char direccion[40];
	char telef[11];
};

typedef struct Contacto Contacto;

struct Cita{
	char fecha[11];
	char lugar[50];
	/* la hora sería del formato hh:mm */
	char horaInicio[6];
	char horaFin[6];
	Contacto *user;
};

typedef struct Cita Cita;

void mostrarDetallesContacto(Contacto *user);

void mostrarDetallesCita(Cita *date);

void mostrarListaContactos(Contacto *contactos);

Contacto buscarContacto(char *nom,char *ape);

Cita buscarCita(char *lugar, char *hora);
