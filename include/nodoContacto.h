typedef struct NodoContacto{
	Contacto *contacto;
	struct NodoContacto *next;
}NodoContacto;

void insertar(NodoContacto **head, Contacto *contacto);
void print_list(NodoContacto *head);
Contacto * getContacto(NodoContacto **head, int n);
void deleteContacto(NodoContacto **head, int n);
Contacto * popContacto(NodoContacto **head);
void modificarContacto(NodoContacto **head, char *nom, char *ape, char *dir, char *cump, char *telf, int n);
Contacto *buscarNodo(NodoContacto **cabecera,char *nombre,char *apellido);
