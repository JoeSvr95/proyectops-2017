#include<stdio.h>
#include"agenda.h"

/* Muestra la información de un contacto siguiendo un formato */
void mostrarDetallesContacto(Contacto *cont){
	printf("Nombre: %s\n", cont->nombre);
	printf("Apellido: %s\n", cont->apellido);
	printf("Fecha de Cumpleaños: %s\n", cont->fechaCump);
	printf("Dirección: %s\n", cont->direccion);
	printf("Telefono: %s\n", cont->telef);
}

/* Muestra la información de una cita siguiendo un formato */
void mostrarDetallesCita(Cita *date){
	printf("Fecha: %s\n", date->fecha);
	printf("Lugar: %s\n", date->lugar);
	printf("Hora inicio: %s\n", date->horaInicio);
	printf("Hora fin: %s\n", date->horaFin);
	if(date->user!=NULL)	
		printf("Persona: %s %s\n", date->user->nombre, date->user->apellido);
}
