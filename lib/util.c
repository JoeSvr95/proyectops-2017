#include"jsmn.h"
#include"agenda.h"
#include"nodoContacto.h"
#include"nodoCita.h"
#include"util.h"
#include<string.h>
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<termios.h>
#include<time.h>

/*Esta funcion abre un archivo con el parametro name, y devuelve un puntero char con toda la informacion del archivo
**Esta funcion es de codigo abierto y fue tomada de:
*
* Autor: Chnossos
* Titulo: Reading text file into char array
* Fecha: 27/03/2014
* Version: Desconocida
* Disponibilidad: https://stackoverflow.com/questions/22697407/reading-text-file-into-char-array
*/
char *copiarArchivo(const char *name){
	char *buffer = NULL;
	size_t size = 0;

	FILE *fp = fopen(name, "r");
	
	if(fp==NULL){
		return buffer;	
	}

	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	
	if(size==0){
		return buffer;	
	}
	rewind(fp);

	buffer = malloc((size) * sizeof(*buffer)); /* size + 1 byte for the \0 */

	fread(buffer, size, 1, fp);
	return buffer;
}

/*Esta funcion utiliza la LinkedList de contactos, haciendo un pop para obtener cada contacto de la lista
* De esta manera se va imprimiendo en formato diccionario una linea con los datos de cada contacto
* Se verifica si el head no es null para no agregar una coma cuando sea el ultimo elemento
*/
int guardarJSONContacto(NodoContacto *head,const char *nombre){
	FILE *f;
	Contacto *d=NULL;
	f=fopen(nombre,"w");
	if(f!=NULL && head!=NULL){
		fprintf(f,"{\"contactos\":[");	
		while(head!=NULL){
			d=popContacto(&head);
			if(head!=NULL){
				fprintf(f,"{\"name\":\"%s\",\"apellido\":\"%s\",\"birthday\":\"%s\",\"telf\":\"%s\",\"dir\":\"%s\"},",d->nombre,d->apellido,d->fechaCump,d->telef,d->direccion);
			}		
			else{
				fprintf(f,"{\"name\":\"%s\",\"apellido\":\"%s\",\"birthday\":\"%s\",\"telf\":\"%s\",\"dir\":\"%s\"}",d->nombre,d->apellido,d->fechaCump,d->telef,d->direccion);
			}		
		}
		fprintf(f,"]}");
		fclose(f);
		free(head);
		return 0;
	}
	return 1;
	
}

/*Esta funcion utiliza la LinkedList de cita, haciendo un pop para obtener cada cita de la lista
* De esta manera se va imprimiendo en formato diccionario una linea con los datos de cada cita
* Se verifica si el head no es null para no agregar una coma cuando sea el ultimo elemento
*/
int guardarJSONCitas(NodoCita *head,const char *nombre){
	FILE *f;
	Cita *c=NULL;
	f=fopen(nombre,"w");
	if(f!=NULL && head!=NULL){
		fprintf(f,"{\"citas\":[");	
		while(head!=NULL){
			c=popCita(&head);
			if(head!=NULL){
				fprintf(f,"{\"fecha\":\"%s\",\"lugar\":\"%s\",\"horaInicio\":\"%s\",\"horaFin\":\"%s\",\"nomCon\":\"%s\",\"apeCon\":\"%s\"},",c->fecha,c->lugar,c->horaInicio,c->horaFin,(c->user!=NULL ? c->user->nombre : ""),(c->user!=NULL ? c->user->apellido : ""));
			}
			else{
				fprintf(f,"{\"fecha\":\"%s\",\"lugar\":\"%s\",\"horaInicio\":\"%s\",\"horaFin\":\"%s\",\"nomCon\":\"%s\",\"apeCon\":\"%s\"}",c->fecha,c->lugar,c->horaInicio,c->horaFin,(c->user!=NULL ? c->user->nombre : ""),(c->user!=NULL ? c->user->apellido : ""));
			}		
		}
		fprintf(f,"]}");
		fclose(f);
		free(head);
		return 0;
	}
	return 1;
}


/*Esta funcion tiene por parametro el puntero del string JSON,un token jsmn a consultar y el parametro de busqueda
**Compara si el token es igual al parametro de busqueda.
**Funcion Tomada del repositorio de la libreria de jsmn.
*/
int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
	if (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
			strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
		return 0;
	}
	return -1;
}


/*Funcion que toma un caracter sin presionar enter, es un paralelismo al getch en windows para Linux,utilizado para reconocer las arrow keys
** para que funcione necesita las librerias <unistd.h> y <termios.h>
** El codigo es codigo abierto tomado de la siguiente referencia:
*
* Autor: mf_
* Titulo: What is Equivalent to getch() & getche() in Linux?
* Fecha: 04/05/2013
* Version: Desconocida
* Disponibilidad: https://stackoverflow.com/questions/7469139/what-is-equivalent-to-getch-getche-in-linux
*/
char getch(){
    char buffer=0;
    struct termios old={0};
    fflush(stdout);
    if(tcgetattr(0, &old)<0)
        perror("tcsetattr()");
    old.c_lflag&=~ICANON;
    old.c_lflag&=~ECHO;
    old.c_cc[VMIN]=1;
    old.c_cc[VTIME]=0;
    if(tcsetattr(0, TCSANOW, &old)<0)
        perror("tcsetattr ICANON");
    if(read(0,&buffer,1)<0)
        perror("read()");
    old.c_lflag|=ICANON;
    old.c_lflag|=ECHO;
    if(tcsetattr(0, TCSADRAIN, &old)<0)
        perror ("tcsetattr ~ICANON");
    return buffer;
 }


/*funcion que tiene como parametro la cabeza de la lista y el tamanio de la misma, muestra los contactos y permite seleccionar
**con las teclas de direccion, si la lista esta vacia, retorna un numero negativo
**El usuario podra cancelar la operacion digitando la letra "e" en cuyo caso se retornara el numero cero.
*/
int seleccionarContacto(NodoContacto *head,int size){
	char c;
	char selecciones[size][10];
	int i,pos;
	NodoContacto *actual;
	strcpy(selecciones[0],"<---\n");
	pos=1;
	if(head!=NULL){	
		for(i=1;i<size;i++)
			strcpy(selecciones[i],"   \n");
		do{	
			system("clear");
			actual=head;
			i=0;
			printf("\n----Seleccione el contacto y presione enter para confirmar o presione \"e\" para cancelar-----\n");
			while(actual!=NULL){
				printf("%d. %s %s",i+1,actual->contacto->nombre,actual->contacto->apellido); printf("%s",selecciones[i]);
				actual=actual->next;
				i++;		
			}
			c=getch();
			if (c  == '\033') { 
	    			getch(); 
	    			switch(getch()) { 
					case 'A':
						if(pos>1){
							strcpy(selecciones[pos-1],"   \n");						
							pos--;
							strcpy(selecciones[pos-1],"<---\n");					
						}
		    				break;
					case 'B':
						if(pos<size){
							strcpy(selecciones[pos-1],"   \n");						
							pos++;
							strcpy(selecciones[pos-1],"<---\n");					
						}
					    	break;
				}
			}
			if(c == 'e')
				return 0;		
		}while(c!='\n');
		return pos;
	}
	return -1;	
}

/*funcion para seleccionar una cita desde un formato tipo calendario.
**El usuario puede navegar con las teclas de direccion las cuales seran capturadas por la funcion getch 
**y usara un switch para cambiar la seleccion. Si el usuario elige un dia presentara una lista con las citas
**disponibles en ese dia, caso contrario se mostrara vacia
**El usuario podra cancelar la operacion digitando la letra "e".
*/
int seleccionarCita(NodoCita *head,int size){
	char c;
	int dias[]={31,28,31,30,31,30,31,31,30,31,30,31};
	char *meses[]={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	int year;
	int mes;
	int dia;
	time_t rawtime;
   	struct tm *tiempo;
	time( &rawtime );
	tiempo = localtime( &rawtime );
	
	year=1900 + tiempo->tm_year;
	mes=tiempo->tm_mon;
	dia=tiempo->tm_mday;
	
	int i;
	NodoCita *actual;
	if(head!=NULL){	
		do{	
			system("clear");
			if(year % 400==0 || ( year % 100!=0 && year % 4==0))
				dias[1]=29;
			else
				dias[1]=28;
			char selecciones[dias[mes]][10];
			for(i=0;i<dias[mes];i++)
				strcpy(selecciones[i],"  ");
			strcpy(selecciones[dia-1],"->");
			printf("\nAnio: %d\n\n\n",year);
			printf("\nMes: %s\n\n\n\n",meses[mes]);
			for(i=0;i<dias[mes];i++){
				if((i+1) % 7!=0)
					printf("%s %d \t\t",selecciones[i],i+1);
				else{
					printf("%s %d\n\n",selecciones[i],i+1);
				}
			}
			printf("\n\nPresione la letra e para salir de la agenda y cancelar la operacion\n");
			c=getch();
			if (c  == '\033') { 
	    			getch(); 
	    			switch(getch()) { 
					case 'A':
						if(dia-7<1){
							mes--;
							if(mes<0){
								year--;
								mes=11;
							}							
							dia=dias[mes];					
						}
						else{
							dia-=7;
						}
		    				break;
					case 'B':
						if(dia+7>dias[mes]){
							mes++;
							dia=1;
							if(mes>11){
								year++;
								mes=0;
							}							
												
						}
						else{
							dia+=7;
						}
					    	break;
					case 'C':
						if(dia+1>dias[mes]){
							mes++;
							dia=1;
							if(mes>11){
								year++;
								mes=0;
							}							
												
						}
						else{
							dia++;
						}
					    	break;
					case 'D':
						if(dia-1<1){
							mes--;
							if(mes<0){
								year--;
								mes=11;
							}	
							dia=dias[mes];						
												
						}
						else{
							dia--;
						}
					    	break;

				}
			}
			else if(c=='\n'){
				char fecha[10];
				char t[3];
				char selec[size][10];
				int posiblesCitas[size];
				int j;
				int pos=1;
				int poslista;
				int encontradas;
				/*convirtiendo datos de fecha a string*/
				sprintf(fecha,(dia>9 ? "%d/":"0%d/"),dia);
				sprintf(t,((mes+1)>9 ? "%d/":"0%d/"),mes+1);
				strcat(fecha,t);
				sprintf(t,"%d",year);
				strcat(fecha,t);
				printf("%s",fecha);
				strcpy(selec[0],"<---\n");
				posiblesCitas[0]=0;
				if(head!=NULL){
					for(j=1;j<size;j++){
						strcpy(selec[j],"   \n");
						posiblesCitas[j]=0;
					}
					do{	
						system("clear");
						actual=head;
						j=0;
						poslista=1;
						encontradas=0;
						printf("\n----Seleccione la cita y presione enter para confirmar-----\n");
						while(actual!=NULL){
							if(strncmp(actual->date->fecha,fecha,strlen(fecha))==0){
								printf("%d. %s %s %s %s",(j+1),actual->date->fecha,actual->date->lugar,actual->date->horaInicio,selec[j]);
								posiblesCitas[j]=poslista;
								j++;
								encontradas++;
							}
							actual=actual->next;
							poslista++;					
						}
						printf("\nencontradas: %d Mueva las teclas segun su opcion\n",encontradas);
						c=getch();
						if (c  == '\033' && encontradas>0) {
				    			getch();
				    			switch(getch()) {
								case 'A':
									if(pos>1){
										strcpy(selec[pos-1],"   \n");
										pos--;
										strcpy(selec[pos-1],"<---\n");
									}
					    				break;
								case 'B':
									if(pos<encontradas){
										strcpy(selec[pos-1],"   \n");
										pos++;
										strcpy(selec[pos-1],"<---\n");
									}
								    	break;
							}
						}
					}while(c!='\n');
					if(encontradas>0){
						printf("%d",posiblesCitas[pos-1]);
						return posiblesCitas[pos-1];
					}		
				}
				else{
					printf("No hay citas disponibles en este dia!!");				
				}
			}	
		}while(c!='e');
		return 0;
	}
	return -1;	
}
