#include"agenda.h"
#include"nodoCita.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/*Esta función agrega una nueva cita a la LinkedList */
void push(NodoCita **head, Cita *cita){
	NodoCita *actual= *head;
	if(*head == NULL){
		*head=malloc(sizeof(NodoCita));
		(*head)->date= cita;
		(*head)->next=NULL;
	}
	else{
		while (actual->next != NULL){
			actual = actual->next;
		}
		actual->next = malloc(sizeof(NodoCita));
		actual->next->date = cita;
		actual->next->next = NULL;
	}
}

/* Esta función imprime todas las citas almacenadas en la LinkedList */
void print_citas(NodoCita *head){
	if(head==NULL){
		printf("\nLISTA VACIA!!\n");
		return;
	}
	int index = 0;
	NodoCita *actual = head;
	while (actual != NULL){
		index++;
		printf("%d. Fecha: %s, Lugar: %s\n", index, actual->date->fecha, actual->date->lugar);
		actual = actual->next;
	}
}

/* Esta función borra una cita de la LinkedList */
void deleteCita(NodoCita **head, int n){
	if(n<=0)
		return;
	n--;
	
	NodoCita *actual = *head;
	NodoCita *temp = NULL;

	if (n == 0){
		temp = actual->next;
		free(*head);
		*head = temp;
	}else{

		for (int i = 0; i<n-1; i++){
			if (actual->next == NULL){
				return;
			}
			actual = actual->next;
		}
	
		temp = actual->next;
		actual->next = temp->next;
		free(temp);
	}
}


/* Esta función modifica una cita con los parámetros enviados. Recibe un índica para saber cuál cita es la */
/* que se modificará */
void modificarCita(NodoCita **head,char *fecha, char *lug, char *inicio, char *fin, int n, Contacto *cont){      
	n--;
        
        NodoCita *actual = *head;
        NodoCita *temp = NULL;
        
        if (n == 0){
		strcpy(actual->date->fecha,fecha); 
                strcpy(actual->date->lugar,lug);
                strcpy(actual->date->horaInicio,inicio);
                strcpy(actual->date->horaFin,inicio);
		if(cont!=NULL)		
			actual->date->user = cont;  
        }else{
                for (int i=0; i<n-1; i++){
                        if (actual->next == NULL){
                                return;
                        }
                        actual = actual->next;
                }
                
                temp = actual->next;
		strcpy(temp->date->fecha,fecha);                
                strcpy(temp->date->lugar,lug);
                strcpy(temp->date->horaInicio,inicio);
                strcpy(temp->date->horaFin,fin);
		if(cont!=NULL)			
			temp->date->user = cont;
        }
}


/* Esta función retorna una cita, recibiendo como párametro el índice de la cita deseada. */
/* La función NO elimina la cita de la LinkedList */
Cita * getCita(NodoCita **head, int n){
	Cita * cita=NULL;	
	if(n<=0)
		return cita;
	n--;
	cita = malloc(sizeof(Cita)) ;
	
	NodoCita *actual = *head;
	NodoCita *temp = NULL;

	if (n == 0){
		return (*head)->date;
	}

	for (int i = 0; i<n-1; i++){
		if (actual->next == NULL){
			return cita;
		}
		actual = actual->next;
	}
	
	temp = actual->next;
	cita = temp->date;
	return cita;
}

/*Esta funcion elimina la primera cita de la lista y lo devuelve, su comportamiento es parecido al de una cola*/
Cita * popCita(NodoCita **head){
	Cita *c = malloc(sizeof(Cita));
	NodoCita *next_nodo = NULL;
	next_nodo = (*head)->next;
	strcpy(c->fecha,(*head)->date->fecha);
	strcpy(c->lugar,(*head)->date->lugar);
	strcpy(c->horaInicio,(*head)->date->horaInicio);
	strcpy(c->horaFin,(*head)->date->horaFin);
	c->user=(*head)->date->user;	
	free(*head);
	*head = next_nodo;
	
	return c;
}
