#include"agenda.h"
#include"nodoContacto.h"
#include"jsmn.h"
#include"util.h"
#include<string.h>
#include<stdlib.h>
#include<stdio.h>

Contacto *c,*d,*temp;
NodoContacto *head = NULL;
NodoContacto *headContacto=NULL;
FILE *f;
char *js;


int main(){
	int i;
	int r;
	jsmn_parser p;
	jsmntok_t t[256];

	head=malloc(sizeof(NodoContacto));
	c= (Contacto *) malloc(sizeof(Contacto));
	strcpy(c->nombre,"Juan");
	strcpy(c->apellido,"Crow");
	strcpy(c->fechaCump,"16/02/1996");
	strcpy(c->telef,"0981512229");
	strcpy(c->direccion,"Asgar av. Saotome");
	printf("The name is: %s %s\n",c->nombre,c->apellido);

	insertar(head, c);
	d = malloc(sizeof(Contacto));
	strcpy(d->nombre,"Joe");
	strcpy(d->apellido,"Saverio");
	strcpy(d->fechaCump,"30/08/1995");
	strcpy(d->telef,"0981796550");
	strcpy(d->direccion,"Urdenor 2 y la que cruza");
	insertar(head,d);
	printf("The name is: %s %s\n",d->nombre,d->apellido);
	printf("The birthday is: %s\n",d->fechaCump);	
	guardarJSONContacto(head,"contactos.json");
	/*free(head);*/
	js=copiarArchivo("contactos.json");	
	printf("%s",js);


	jsmn_init(&p);
	r = jsmn_parse(&p, js, strlen(js), t, sizeof(t)/sizeof(t[0]));
	if (r < 0) {
		printf("Failed to parse JSON: %d\n", r);
		return 1;
	}

	/* Assume the top-level element is an object */
	if (r < 1 || t[0].type != JSMN_OBJECT) {
		printf("Object expected\n");
		return 1;
	}
	printf("\nSucessful tokenizing\n");
	
	headContacto=malloc(sizeof(NodoContacto));
	headContacto->contacto=NULL;
	for (i = 1; i < r; i++){
		if (jsoneq(js, &t[i], "name") == 0) {			
			temp=malloc(sizeof(Contacto));
			strncpy(temp->nombre,js + t[i+1].start,t[i+1].end-t[i+1].start);
			printf("NAME: %s\n",temp->nombre);			
			/*printf("nombre: %.*s\n", t[i+1].end-t[i+1].start, js + t[i+1].start);*/
			i+=2;
			strncpy(temp->apellido,js + t[i+1].start,t[i+1].end-t[i+1].start);
			printf("APELLIDO: %s\n",temp->apellido);			
			i+=2;
			strncpy(temp->fechaCump,js + t[i+1].start,t[i+1].end-t[i+1].start);
			printf("BIRTH: %s\n",temp->fechaCump);			
			i+=2;
			strncpy(temp->telef,js + t[i+1].start,t[i+1].end-t[i+1].start);
			printf("TELF: %s\n",temp->telef);			
			i+=2;
			strncpy(temp->direccion,js + t[i+1].start,t[i+1].end-t[i+1].start);
			printf("DIR: %s\n",temp->direccion);
			insertar(headContacto, temp);			
			i++;
			printf("\nEnd of parsed info\n");	
		}
	}
	print_list(headContacto);
	/*
	printf("Printing list....!\n");
	printf("head: %s\n",head->contacto->nombre);
	print_list(head);
	printf("Tomando el contaco Joe B)\n");
	c = getContacto(&head, 2);
	printf("%s\n", c->nombre);
	print_list(head);
	printf("Eliminando a Juan :v\n");
	deleteContacto(&head, 1);
	print_list(head);
	printf("Insertando un contacto nuevo\n");
	d = (Contacto *) malloc(sizeof(Contacto));
	strcpy(c->nombre,"Elver");
	strcpy(c->apellido,"Galarga");
	strcpy(c->fechaCump,"06/06/2006");
	strcpy(c->telef,"0987654321");
	strcpy(c->direccion,"Guasmo Sur");
	insertar(head, d);
	print_list(head);
	*/
}

