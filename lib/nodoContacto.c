#include"agenda.h"
#include"nodoContacto.h"
#include<stdlib.h>
#include<stdio.h>
#include<string.h>

/*Esta funcion inserta un nuevo nodo al final de la lista con el parametro de contacto como contenido*/
void insertar(NodoContacto **head, Contacto *cont){
	NodoContacto *actual =*head;
	if (*head == NULL){
		*head=malloc(sizeof(NodoContacto));
		(*head)->contacto = cont;
		(*head)->next = NULL;
	}else{
		while (actual->next != NULL){
			actual = actual->next;
		}
		actual->next = malloc(sizeof(NodoContacto));
		actual->next->contacto = cont;
		actual->next->next = NULL;
	}
}

/*Esta funcion imprime todos los contactos de la lista  con indices y mostrando todos sus datos*/
void print_list(NodoContacto *head){
	if(head==NULL){
		printf("\nLISTA VACIA!!!\n");
		return;
	}
	int index = 0;
	NodoContacto *actual = head;
	while (actual != NULL){
		index++;
		printf("%d. %s %s %s %s %s\n",index, actual->contacto->nombre,actual->contacto->apellido,actual->contacto->fechaCump,actual->contacto->telef,actual->contacto->direccion);
		actual = actual->next;
	}
}

/*Esta funcion obtiene un contacto pasando como parametro el indice en la lista*/
Contacto * getContacto(NodoContacto **head, int n){
	Contacto *cont=NULL;	
	if(n<=0)
		return cont;	
	n--;
	cont = malloc(sizeof(Contacto)) ;
	
	NodoContacto *actual = *head;
	NodoContacto *temp = NULL;

	if (n == 0){
		return (*head)->contacto;
	}

	for (int i = 0; i<n-1; i++){
		if (actual->next == NULL){
			return cont;
		}
		actual = actual->next;
	}
	
	temp = actual->next;
	cont = temp->contacto;
	return cont;
}

/* Esta funcion elimina un contacto pasando como parametro el indice en la lista*/
void deleteContacto(NodoContacto **head, int n){
	if(n<=0)
		return;	
	n--;
	
	NodoContacto *actual = *head;
	NodoContacto *temp = NULL;

	if (n == 0){
		temp = actual->next;
		free(*head);
		*head = temp;
	}else{

		for (int i = 0; i<n-1; i++){
			if (actual->next == NULL){
				return;
			}
			actual = actual->next;
		}
	
		temp = actual->next;
		actual->next = temp->next;
		free(temp);
	}
}

/* Esta funcion modifica los datos de un Contacto en un nodo de la lista pasando tambien como parametro
 * El indice en la lista
 */
void modificarContacto(NodoContacto **head, char *nom, char *ape, char *cump, char *dir, char *telf, int n){
        n--;
        
        NodoContacto *actual = *head;
        NodoContacto *temp = NULL;
        
        if (n == 0){
                strcpy(actual->contacto->nombre,nom);
                strcpy(actual->contacto->apellido,ape);
                strcpy(actual->contacto->fechaCump,cump);
                strcpy(actual->contacto->direccion,dir);
                strcpy(actual->contacto->telef,telf);   
        }else{
                for (int i=0; i<n-1; i++){
                        if (actual->next == NULL){
                                return;
                        }
                        actual = actual->next;
                }
                
                temp = actual->next;
                strcpy(temp->contacto->nombre,nom);
                strcpy(temp->contacto->apellido,ape);
                strcpy(temp->contacto->fechaCump,cump);
                strcpy(temp->contacto->direccion,dir);
                strcpy(temp->contacto->telef,telf);
        }
}

/*Esta funcion devuelve el contacto de un nodo, para encontrarlo compara el nombre y el apellido de  
 *del contacto de la lista con el contacto de parametro
 */
Contacto *buscarNodo(NodoContacto **cabecera,char *nombre,char *apellido){
	NodoContacto *actual;
	Contacto *contBuscado=NULL;
	actual=*cabecera;
	while(actual!=NULL){
		if(strncmp(actual->contacto->nombre,nombre,strlen(nombre))==0 && strncmp(actual->contacto->apellido,apellido,strlen(apellido))==0){
			contBuscado=actual->contacto;
			return contBuscado;
		}
		actual=actual->next;
	}
	return contBuscado;
}

/*Esta funcion elimina el primer contacto de la lista y lo devuelve, su comportamiento es parecido al de una cola*/
Contacto * popContacto(NodoContacto **head){
	Contacto *cont = malloc(sizeof(Contacto));
	NodoContacto *next_nodo = NULL;
	next_nodo = (*head)->next;
	strcpy(cont->nombre,(*head)->contacto->nombre);
	strcpy(cont->apellido,(*head)->contacto->apellido);
	strcpy(cont->fechaCump,(*head)->contacto->fechaCump);
	strcpy(cont->direccion,(*head)->contacto->direccion);
	strcpy(cont->telef,(*head)->contacto->telef);
	free(*head);
	*head = next_nodo;
	
	return cont;
}


