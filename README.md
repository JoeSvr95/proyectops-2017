# Programa de agenda de citas y contactos. #
### Integrantes: Juan José Crow Washbrum y Joe Sebastián Saverio ###

## Funcionamiento
 ##
Este programa le permite al usuario poder agregar nuevos contactos y modificarlos su información cuando quiera. También permie agendar citas con dichos contactos y poder revisar éstas en un formato de calenadario.

El programa primero muestra un menú principal con las opciones de "Contactos" y "Citas". En el menú de Contactos se encuentran todas las funciones relacionadas a los contactos. Agregar, Modificar, Borrar, Listar y Mostrar detalles de los contactos. La opción mostrar detalles de contactos muestra todo la información del contacto, mientras que la de Listar muestra una lista de todos los contactos del sistema.

El menú de citas es similar. Se muestran las mismas opciones que en el menú Contactos, solo que esta vez relacionado a las citas. Para Modificar, Borrar y Mostrar detalles se muestra un calendario. El usuario tendrá que escoger el día de la cita y luego se le mostrarán todas las citas pendientes para ese día. Luego selecciona una de estas citas y realiza la operación correspondiente a la opción que eligió. Si eligió modificar, el programa le pedirá que ingrese los nuevos datos para esa cita. Si eligió Borrar, la cita seleccionada se eliminará del sistema y si eligió Mostrar Detalles, se mostrará la información de la cita.

Cuando el usuario escoge la opción de salir, todos los cambios realizados y los nuevos contactos o citas agregadas se guardarán automáticamente.

## ¿Como utilizarlo? ##

Se puede clonar directamente nuestro directorio y compilar el proyecto con el Makefile incluido, se generará un ejecutable llamado agenda el cual es el ejecutable del programa.

## Notas adicionales ##

Este proyecto utiliza librerias y funciones de terceros:

-JSMN
-getch()
-copiarArchivo()

Nota 1: La función getch() extraida externamente ha sido probada exclusivamente para Linux, por lo que no hay garantía de que su funcionamiento sea correcto en Windows, en vez de esto, podría modificar nuestro código fuente para que las librerias no utilicen el getch externo sino el getch que existe en la librería conio.h de Windows

Nota 2:El archivo proof.c fue utilizado unicamente para testear las librerías externas, por lo que no tiene ninguna funcionalidad importante en el proyecto.