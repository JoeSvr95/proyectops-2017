IDIR =./include
CC=gcc
CFLAGS=-g -I$(IDIR)

ODIR=obj
LDIR =./lib

LIBS=./lib/libjsmn.a -lm

_DEPS = nodoContacto.h nodoCita.h agenda.h jsmn.h util.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = nodoContacto.o nodoCita.o agenda.o util.o
OBJ = $(patsubst %,$(LDIR)/%,$(_OBJ)) ./src/main.c


%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

agenda: $(OBJ)
	gcc -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ 
