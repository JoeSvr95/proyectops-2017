#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include"agenda.h"
#include"nodoContacto.h"
#include"nodoCita.h"
#include"jsmn.h"
#include"util.h"

int main(){
	/*variables de LinkedLists*/
	NodoContacto *headContacto;
	int sizeContactos;
	NodoCita *headCita;
	int sizeCitas;
	/*variables para opciones menu*/
	int opMenu;
	int opCon;
	int opCita;
	int seleccion;
	int sel2;
	/* Contacto */
	char nomCon[15];
	char apeCon[20];
	char fNacCon[11];
	char telefCon[10];
	char direccion[50];
	/* Cita */
	char fecha[11];
	char lugar[50];
	char inicio[6];
	char fin[6];
	/*Punteros para funciones de Citas*/
	Cita *nuevaCita;
	Cita *citaConsulta;
	NodoCita *nodoConsultaCita;
	/*Punteros para funciones de Contactos*/
	Contacto *nuevoContacto;
	Contacto *contactoConsulta;
	/*variables para parseo de json*/
	int i;
	int r;
	jsmn_parser p;
	jsmntok_t t[256];
	char *js;
	char *jsCitas;
	Contacto *temp;
	Cita *ctemp;
	Contacto *conCita;
	char nombreBusqueda[15];
	char apeBusqueda[20];

	/*bloque para cargar archivo json, parsearlo y pasar la informacion a la lista*/
	js=copiarArchivo("contactos.json");
	jsCitas=copiarArchivo("citas.json");	
	/*headContacto=malloc(sizeof(NodoContacto));*/
	sizeContactos=0;
	sizeCitas=0;
	/*bloque para cargar desde json de contactos*/	
	if(js!=NULL){
		jsmn_init(&p);
		r = jsmn_parse(&p, js, strlen(js), t, sizeof(t)/sizeof(t[0]));
		if (r < 0) {
			printf("Failed to parse JSON: %d\n", r);
		}


		if (r < 1 || t[0].type != JSMN_OBJECT) {
			printf("Object expected\n");
		}
		printf("\nSucessful tokenizing\n");

		for (i = 1; i < r; i++){
			if (jsoneq(js, &t[i], "name") == 0) {			
				temp=malloc(sizeof(Contacto));
				strncpy(temp->nombre,js + t[i+1].start,t[i+1].end-t[i+1].start);
				printf("NAME: %s\n",temp->nombre);
				i+=2;
				strncpy(temp->apellido,js + t[i+1].start,t[i+1].end-t[i+1].start);
				printf("APELLIDO: %s\n",temp->apellido);			
				i+=2;
				strncpy(temp->fechaCump,js + t[i+1].start,t[i+1].end-t[i+1].start);
				printf("BIRTH: %s\n",temp->fechaCump);			
				i+=2;
				strncpy(temp->telef,js + t[i+1].start,t[i+1].end-t[i+1].start);
				printf("TELF: %s\n",temp->telef);			
				i+=2;
				strncpy(temp->direccion,js + t[i+1].start,t[i+1].end-t[i+1].start);
				printf("DIR: %s\n",temp->direccion);
				insertar(&headContacto, temp);
				sizeContactos++;			
				i++;
				printf("\nEnd of parsed info\n");	
			}
		}
	print_list(headContacto);
	}
	else{
		printf("\nNo se ha podido cargar el archivo de contactos o no hay info guardada\n");
	}	
		
	/*Bloque de cargado de contactos desde el json*/
	if(jsCitas!=NULL){
		jsmn_init(&p);
		r = jsmn_parse(&p, jsCitas, strlen(jsCitas), t, sizeof(t)/sizeof(t[0]));
		if (r < 0) {
			printf("Failed to parse JSON: %d\n", r);
		}

		/* Assume the top-level element is an object */
		if (r < 1 || t[0].type != JSMN_OBJECT) {
			printf("Object expected\n");
		}
		printf("\nSucessful tokenizing\n");

		for (i = 1; i < r; i++){
			if (jsoneq(jsCitas, &t[i], "fecha") == 0) {	
				ctemp=malloc(sizeof(Cita));
				strncpy(ctemp->fecha,jsCitas + t[i+1].start,t[i+1].end-t[i+1].start);
				printf("FECHA: %s\n",ctemp->fecha);
				i+=2;
				strncpy(ctemp->lugar,jsCitas + t[i+1].start,t[i+1].end-t[i+1].start);
				printf("LUGAR: %s\n",ctemp->lugar);			
				i+=2;
				strncpy(ctemp->horaInicio,jsCitas + t[i+1].start,t[i+1].end-t[i+1].start);
				printf("INICIO: %s\n",ctemp->horaInicio);			
				i+=2;
				strncpy(ctemp->horaFin,jsCitas + t[i+1].start,t[i+1].end-t[i+1].start);
				printf("FIN: %s\n",ctemp->horaFin);			
				i+=2;
				strncpy(nombreBusqueda,jsCitas + t[i+1].start,t[i+1].end-t[i+1].start);
				i+=2;
				strncpy(apeBusqueda,jsCitas + t[i+1].start,t[i+1].end-t[i+1].start);
				if(*nombreBusqueda && *apeBusqueda){
					conCita=buscarNodo(&headContacto,nombreBusqueda,apeBusqueda);
					if(conCita!=NULL){
						ctemp->user=conCita;					
					}
				}
				printf("FECHA: %s\n",ctemp->fecha);				
				push(&headCita, ctemp);
				sizeCitas++;			
				i++;
				printf("\nEnd of parsed info\n");	
			}
		}
	print_citas(headCita);
	}
	else{
		printf("\nNo se ha podido cargar el archivo de citas o no hay info guardada\n");
	}
	
	/*Se muestra el menú principal para que el usuario escoja qué desea revisar */
	while(opMenu!=3){
		printf("\n======Agenda Personal======\n");
		printf("1.	Contactos.\n");
		printf("2.	Citas.\n");
		printf("3. Salir\n");
		printf("Escoja una opción del menú: ");
		scanf("%d",&opMenu);
		switch(opMenu){
			case 1:
				/* En este submenú el usuario puede realizar todas las tareas relacionadas a los contactos 
				   como crear nuevo contacto, modificar contacto, entre otros. */
				printf("\n======Contactos======\n");
				printf("1.	Crear Contacto.\n");
				printf("2.	Modificar Contacto.\n");
				printf("3.	Borrar Contacto.\n");
				printf("4.	Listar Contactos.\n");
				printf("5.	Mostrar detalles de Contacto.\n");
				printf("Ingrese una opción del menú: ");
				scanf("%d",&opCon);
				switch(opCon){
					case 1:
						/* Se piden todos los datos necesarios para crear un nuevo contacto */
						printf("\n===Creando nuevo Contacto===\n");
						printf("Ingrese nombre: ");
						scanf("%s",nomCon);
						printf("Ingrese apellido: ");
						scanf("%s",apeCon);
						printf("Ingrese fecha de cumpleaños (dd/mm/yyyy): ");
						scanf("%s",fNacCon);
						printf("Ingrese telefono: ");
						scanf("%s",telefCon);
						printf("Ingrese la direccion: ");
						while(getchar() != '\n');
						scanf("%[^\n]s",direccion);
						nuevoContacto = (Contacto *)malloc(sizeof(Contacto));
						if (nuevoContacto != NULL){
							strncpy(nuevoContacto->nombre,nomCon,strlen(nomCon));
							strncpy(nuevoContacto->apellido,apeCon,strlen(apeCon));
							strncpy(nuevoContacto->fechaCump,fNacCon,strlen(fNacCon));
							strncpy(nuevoContacto->telef,telefCon,strlen(telefCon));
							strncpy(nuevoContacto->direccion,direccion,strlen(direccion));
							insertar(&headContacto,nuevoContacto);
							sizeContactos++;
							print_list(headContacto);
							printf("\nContacto agregado con exito!\n");
						}else{
							printf("No se pudo ingresar el nuevo contacto\n");
						}
						while(getchar() != '\n');
						break;
					case 2:
						/* En esta sección se muestran todos los contactos del sistema mediante la función 
						   seleccionarContacto y el usuario selecciona uno para ser modíficado. Luego se le
						   pide que ingrese todos los datos nuevos. */
						printf("\n======Modificar usuario======\n");
						seleccion=seleccionarContacto(headContacto,sizeContactos);
						if(seleccion>0){
							printf("\nIngrese nuevo nombre: ");
							scanf("%s",nomCon);
							printf("Ingrese nuevo apellido: ");
							scanf("%s",apeCon);
							printf("Ingrese nueva fecha de cumpleaños: ");
							scanf("%s",fNacCon);
							printf("Ingrese nuevo telefono: ");
							scanf("%s",telefCon);
							printf("Ingrese direccion: ");
							while(getchar() != '\n');
							scanf("%[^\n]s",direccion);
							modificarContacto(&headContacto,nomCon,apeCon,fNacCon,direccion,telefCon,seleccion);
							printf("\nContacto modificado con exito!\n");
							print_list(headContacto);
						}
						else if(seleccion==0){
							printf("\nOperacion Cancelada por el usuario\n");
						}
						else{
							printf("La lista esta vacia o ocurrio algun error");
						}
						while(getchar() != '\n');
						break;
					case 3:
						/* Se vuelve a mostrar todos los contactos y el usuario escoge el contacto que desea eliminar.
						   La función deleteContacto borra el contacto de la LinkedList y del sistema */
						printf("======Borrar Contacto======");
						seleccion=seleccionarContacto(headContacto,sizeContactos);
						if(seleccion>0){						
							deleteContacto(&headContacto,seleccion);
							sizeContactos--;
							printf("\nContacto eliminado exitosamente!!\n");
							print_list(headContacto);
						}
						else if(seleccion==0){
							printf("\nOperacion Cancelada por el usuario\n");
						}
						else{
							printf("La lista esta vacia o ocurrio algun error");
						}
						break;
					case 4:
						/* En esta sección se listan todos los contactos que halla en el sistema */
						printf("\n======Listar Contactos======\n");
						printf("\n A continuacion se muestra los contactos...\n");
						printf("============================\n");
						print_list(headContacto);
						printf("============================\n");
						break;
					case 5:
						/* Se muestran los contactos del sistema y el usuario selecciona para ver la información completa
						   del contacto */
						printf("======Mostrar detalles de Contacto======");
						seleccion=seleccionarContacto(headContacto,sizeContactos);
						if(seleccion>0){				
							printf("\nMostrando informacion del contacto....\n");
							printf("============================\n");
							contactoConsulta=getContacto(&headContacto,seleccion);		
							mostrarDetallesContacto(contactoConsulta);
							printf("\n-----Citas pendientes:\n");
							nodoConsultaCita=headCita;
							while(nodoConsultaCita!=NULL){
								if(nodoConsultaCita->date->user!=NULL && strcmp(nodoConsultaCita->date->user->nombre,contactoConsulta->nombre)==0 && strcmp(nodoConsultaCita->date->user->apellido,contactoConsulta->apellido)==0)
								{
									printf("\nLugar: %s\n",nodoConsultaCita->date->lugar);
									printf("Fecha: %s\n",nodoConsultaCita->date->fecha);
									printf("hora de Inicio: %s\n",nodoConsultaCita->date->horaInicio);
									printf("hora de fin: %s\n\n",nodoConsultaCita->date->horaFin);
								}
								nodoConsultaCita=nodoConsultaCita->next;
							}
							printf("============================");
						}
						else if(seleccion==0){
							printf("\nOperacion Cancelada por el usuario\n");
						}
						else{
							printf("La lista esta vacia o ocurrio algun error");
						}						
						break;
					default:
						printf("Ingrese una opción del menu: \n");
						scanf("%d",&opCon);		
				}
				break;
			case 2:
				/* En esta sección el usuario puede realizar todas las tareas relacionadas a las citas */
				printf("\n======Cita======\n");
				printf("1.	Crear Cita.\n");
				printf("2.	Modificar Cita.\n");
				printf("3.	Borrar Cita.\n");
				printf("4.	Mostrar Citas.\n");
				printf("5.	Mostrar detalles de Cita.\n");
				printf("Ingrese una opción del menú: ");
				scanf(" %d",&opCita);
				switch(opCita){
					case 1:
						/* Se le pide al usuario ingresar todos los datos para crear una cita. El lugar, fecha, hora de inicio de la cita y hora fin. También se le pide seleccionar el contacto con el que tendrá la cita */
						printf("\n===Creando nueva Cita===\n");
						printf("Ingrese la fecha de la cita (dd/mm/yyyy): ");
						scanf("%s",fecha);
						printf("Ingrese lugar: ");
						while(getchar() != '\n');
						scanf("%[^\n]s",lugar);
						printf("Ingrese hora de inicio (hh:mm): ");
						scanf("%s",inicio);
						printf("Ingrese hora fin (hh:mm): ");
						scanf("%s",fin);
						seleccion=seleccionarContacto(headContacto,sizeContactos);
						contactoConsulta = getContacto(&headContacto,seleccion);
						nuevaCita = (Cita *) malloc(sizeof(Cita));
						if (nuevaCita != NULL){
							strcpy(nuevaCita->fecha,fecha);
							strcpy(nuevaCita->lugar, lugar);
							strcpy(nuevaCita->horaInicio, inicio);
							strcpy(nuevaCita->horaFin,fin);
							if(contactoConsulta!=NULL)							
								nuevaCita->user = contactoConsulta;
							push(&headCita, nuevaCita);
							printf("Cita en %s creada!", nuevaCita->lugar);
							sizeCitas++;
							print_citas(headCita);
						}
						while(getchar() != '\n');
						break;
					case 2:
						/* Se muestran las citas del sistema por medio de un calendario mediante la función seleccionarCita. El usuario selecciona el día y luego la cita, después se le pide ingresar todos los datos nuevos de la cita. */
						printf("\n======Modificar Cita======\n");
						seleccion = seleccionarCita(headCita,sizeCitas);
        					if (seleccion>0){
                					printf("Ingrese nueva fecha: ");
                					scanf("%s",fecha);
                					printf("Ingrese nuevo lugar: ");
							while(getchar() != '\n');
                					scanf("%[^\n]s",lugar);
                					printf("Ingrese nueva hora de inicio (hh:mm): ");
                					scanf("%s",inicio);
                					printf("Ingrese nueva hora fin (hh:mm)): ");
                					scanf("%s",fin);
                					sel2 = seleccionarContacto(headContacto,sizeContactos);
                					contactoConsulta = getContacto(&headContacto,sel2);
                					modificarCita(&headCita,fecha,lugar,inicio,fin,seleccion,contactoConsulta);
                					printf("Cita modificada con exito!\n");
        					}else{
                					printf("Ninguna cita seleccionada\n");
        					}
        					while(getchar() != '\n');
       	 					break;
					case 3:
						/* Se muestran todas las citas del sistema por medio de un calendario. El usuario selecciona el día 
						   y se muestran las citas de ese día. El usuario selecciona la cita que quiera eliminar y luego 
						   se elimina de la LinkedList y del sistema. */
						printf("======Borrar Cita======\n");
						seleccion=seleccionarCita(headCita,sizeCitas);
						if(seleccion>0){
							deleteCita(&headCita,seleccion);
							sizeCitas--;
							printf("\nCita eliminada con exito!!!\n");
						}
						else if(seleccion==0){
							printf("\nOperacion cancelada por el usuario\n");	
						}
						else{
							printf("\nError en operacion o lista esta vacia!!\n");
						}
						break;
					case 4:
					/* Se muestra una lista de todas las citas que halla en el sistema, Se muestra la fecha y el lugar */
						printf("\n======Listar Citas======\n");
						printf("A continuación se muestran todas las citas...\n");
						print_citas(headCita);
						break;
					case 5:
						/* Se muestra un calendario donde el usuario selecciona el día. Se muestran todas las citas que halla
						   en ese día y el usuario selecciona la cita para ver la información completa de la Cita. */
						printf("\n======Mostrar detalles de una Cita======\n");
						seleccion = seleccionarCita(headCita,sizeCitas);
						if (seleccion>0){
							citaConsulta = getCita(&headCita,seleccion);
					   		mostrarDetallesCita(citaConsulta);
						}
						else if(seleccion==0){
							printf("\nOperacion cancelada por el usuario\n");
						}
						else{
							printf("Error en la operación o lista vacía!\n");
						}
						break;
					default:
						break;		
				}
				break;
			case 3:
				printf("\n*****SALIENDO DEL SISTEMA DE AGENDAS!!******\n");
				break;
			default:
				printf("Ingrese una opcion correcta");
				break;
		}
	}
	printf("\nGuardando datos en json...\n");
	guardarJSONContacto(headContacto,"contactos.json");
	guardarJSONCitas(headCita,"citas.json");
	printf("Guardado finalizado!\n");
}
